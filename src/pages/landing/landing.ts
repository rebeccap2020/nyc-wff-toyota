import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {
  isMobile: boolean;
  oneClick: boolean = false;
  width: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.width = window.innerWidth;
    console.log('landing page');
    if (window.innerWidth < 505) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
    console.log(this.isMobile);
  }

  next() {
    this.navCtrl.push('InfoPage');
  }

}
