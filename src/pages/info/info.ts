import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { state } from '@angular/core/src/animation/dsl';

@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {

  errorMessage
  data:any = this.navParams.data;
  userInfo = {
    first_name: '',
    last_name: '',
    email: '',
    address1: '',
    address2: '',
    city: '',
    state: '',
    zip: '',
    size: '',
    country: 'United States',
    country_short: ''
  };
  autocomplete: { input: string; };
  autocompleteItems: any[];
  location: any;
  placeid: any;
  GoogleAutocomplete: any;
  GooglePlaces: any;
  sizes =[];
  acceptTerms = false;
  isMobile: boolean;
  soldOut:boolean = false;
  address_detail: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public modalCtrl: ModalController, public zone: NgZone,) {
    this.GooglePlaces = new google.maps.places.PlacesService(document.createElement("div"));
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
    // this.navCtrl.push('LandingPage');
  }
  
    ionViewDidLoad() {
        this.http.post('https://fi.swdlab.com/_ajax/toyota_eat_repeat2020?dowhat=count', JSON.stringify(this.data)).subscribe((ret:any)=>{
            if(ret && parseInt(ret.clean[0]) >= 2000) {
            this.soldOut = true;
            } else {
            this.soldOut = false;
            }
        });
        if(Object.keys(this.navParams.data).length < 1) {
            this.data = {
            item: 'oven mitt'
            };
        }
        if (window.innerWidth < 505) {
            this.isMobile = true;
        } else {
            this.isMobile = false;
        }
        // console.log('This promotion has ended!');
    }

    next() {
        let u = this.userInfo;
        if(u.first_name && u.last_name && u.email && u.address1 && u.city && u.zip) {
            this.data.user = this.userInfo;
            this.data.item = this.data.item;
            this.navCtrl.push('ConfirmPage', this.data);
        }
        else this.errorMessage = 'Please fill out all the fields!';
        // console.log('This promotion has ended!');
    }

    UpdateSearchResults(){
        console.log('update search result');
        if (this.userInfo.address1 == '') {
            this.autocompleteItems = [];
            return;
        }
        else if (this.userInfo.address1 == this.autocomplete.input) {
            this.autocompleteItems = [];
        }
        else {
            this.GoogleAutocomplete.getPlacePredictions({ input: this.userInfo.address1, types: ['address'] },
            (predictions, status) => {
                this.autocompleteItems = [];
                this.zone.run(() => {
                    predictions.forEach((prediction) => {
                    this.autocompleteItems.push(prediction);
                    });
                });
            });
        }
    }
    
    SelectSearchResult(item) {
    this.placeid = item.place_id;
    const request = {
        placeId: this.placeid,
        fields: ["name", "formatted_address", "place_id", "address_components"]
    };

    this.GooglePlaces.getDetails(request, (place, status) => {
        console.log('details', place);
        console.log(place.formatted_address.split(','));
        var split_address = place.formatted_address.split(',');
        this.address_detail = place.address_components;
        for(let i = 0; i < this.address_detail.length; i++) {
            if (this.address_detail[i]['types'].includes('country')) {
                if (this.address_detail[i]['long_name'] == 'United States') {
                    this.autocomplete.input = split_address[0];
                    this.userInfo.address1 = split_address[0];
                    this.userInfo.city = split_address[1].trim();
                    var state_zip_clean = split_address[2].trim();
                    var state_zip = state_zip_clean.split(' ');
                    this.userInfo.state = state_zip[0];
                    this.userInfo.zip = state_zip[1];
                    this.userInfo.country = 'US';
                    this.userInfo.country_short = 'US';
                    console.log('after formatting us address', this.userInfo);
                }
                else {
                    this.autocomplete.input = split_address[0];
                    this.userInfo.address1 = split_address[0];
                    var state_zip_clean = split_address[1].trim();
                    var state_zip = state_zip_clean.split(' ');
                    this.userInfo.city = state_zip[1];
                    this.userInfo.zip = state_zip[0];
                    this.userInfo.country = this.address_detail[i]['long_name'];
                    console.log('after formatting international address', this.userInfo);
                }
            }
        }
        this.ClearAutocomplete();
    });
    }
    
    ClearAutocomplete(){
    this.autocompleteItems = [];
    // this.autocomplete.input = '';
    }

    blurFunction() {
        setTimeout(() => this.ClearAutocomplete(), 500);
    }

}
