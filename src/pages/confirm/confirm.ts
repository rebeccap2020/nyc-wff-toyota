import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-confirm',
  templateUrl: 'confirm.html',
})
export class ConfirmPage {

  httpOptions = {
    headers: new HttpHeaders()
  };
  data = this.navParams.data;
  isMobile: boolean;
  oneClick: boolean = false;
  width: number;
  alreadySubmitted:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, private alertCtrl: AlertController, public loadingController: LoadingController) {}

  ionViewDidLoad() {
    // this.navCtrl.push('LandingPage');
    this.width = window.innerWidth;
    console.log('confirm page nav params', this.navParams.data);
    if (window.innerWidth < 505) {
        this.isMobile = true;
    } else {
        this.isMobile = false;
    }
    if(this.data) {
        if(!this.data.user || !this.data.item) { 
            this.navCtrl.setRoot('InfoPage')
        }
        
        this.http.get('https://restcountries.eu/rest/v2/name/'+this.data.user.country).subscribe((ret:any)=>{
            console.log('getting country code', ret.alpha2Code);
            this.navParams.data.user.country_short = ret.alpha2Code;
        });
    } else {
        this.navCtrl.setRoot('InfoPage');
    // console.log('This promotion has ended!');
    }
  }
  
  submit(){
    this.oneClick = true;
    const loading = this.loadingController.create({
      content: 'Please wait...'
    });
    loading.present();
    this.http.post('https://fi.swdlab.com/_ajax/toyota_eat_repeat_2020?dowhat=submit', JSON.stringify(this.data), this.httpOptions).subscribe((ret:any)=>{
      console.log('ret',ret);
      if (ret.result == 'error') {
        this.alreadySubmitted = true;
        let alert = this.alertCtrl.create({
          title: 'Sorry',
          subTitle: ret.error,
          buttons: ['Dismiss']
        });
        loading.dismiss();
        alert.present();
      } else {
        loading.dismiss();
        localStorage.setItem('submit', (localStorage.getItem('submit') ? parseInt(localStorage.getItem('submit')) + 1 : 1) + '');
        this.navCtrl.push('ThanksPage', this.data);
      }
    });
    // console.log('this has been submitted');
    // this.navCtrl.push('ThanksPage', this.data);
  }
  // console.log('This promotion has ended!');
}
