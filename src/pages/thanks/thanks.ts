import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-thanks',
  templateUrl: 'thanks.html',
})
export class ThanksPage {

  data = this.navParams.data;
  isMobile:boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // this.navCtrl.push('LandingPage');
  }

  ionViewDidLoad() {
    // console.log('This promotion has ended.');
    if (window.innerWidth < 505) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
    console.log(this.isMobile);
    setTimeout(()=>window.open('https://pantheon.io/', '_self'),5000);
  }

}
